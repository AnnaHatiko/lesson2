const EventEmitter = require('events');

class ChatApp extends EventEmitter {
    /**
     * @param {String} title
     */
    constructor(title) {
        super();

        this.title = title;

// Посылать каждую секунду сообщение
        setInterval(() => {
            this.emit('message', `${this.title}: ping-pong`);
    }, 1000);
    }

    close() {
        this.emit('close');
        this.removeAllListeners();
    }
}

let webinarChat = new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat = new ChatApp('---------vk');

let chatOnMessage = (message) => {
    console.log(message);
};

webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);


// Закрыть вконтакте
let vkTimeInterval = setTimeout( ()=> {
    console.log('Закрываю вконтакте...');
vkChat.removeListener('message', chatOnMessage);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
    console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.removeListener('message', chatOnMessage);
}, 100 );

// Закрыть webinar
setTimeout( ()=> {
    console.log('Закрываю вебинар!');
    webinarChat.removeListener('message', chatOnMessage);
}, 30000 );

//part_1
//Обработчик
let prepareToAnswer = () => {
    console.log('Готовлюсь к ответу');
}

//Добавить обработчик
webinarChat.prependListener('message', prepareToAnswer);
vkChat.setMaxListeners(2);
vkChat.prependListener('message', prepareToAnswer);

//part_2
vkChat.on('close', function(){
    console.log('Чат вконтакте закрылся :(');
});
vkChat.close();
clearTimeout(vkTimeInterval);